# Playback

This is an old high school final project that was built circa 2013. It is a GUI
front end for Windows that opens audio files using a library called
[BASS](https://www.un4seen.com/). It was developed using C++ and Visual Studio
.NET frameworks for Windows 7. I found the files lying around on an old
harddrive around 2016 and decided to resurrect my high school code for fun and
nostalgia purposes

## Getting Started

### Dependencies

* Windows 7 or above (haven't tested on above)
* [Visual Studio (Full Version. Not Code)](https://visualstudio.microsoft.com/)

### Build Instructions

* Open Playback.sln in VS
* Hit build and run
* ???
* Profit. hopefully. maybe. idk
